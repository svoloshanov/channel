import UIKit

final class ProgramCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var programView: UIView!
    @IBOutlet weak var programImageView: UIImageView!
    @IBOutlet weak var programLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        programView.viewCorner(18)
    }
    
    func setupImage(_ url: String?) {
        guard let url = url else { return }
        
        //setup image from  url
    }
    
    func setupLabel(_ text: String?) {
        guard let text = text else { return }
        
        //setup text to label
    }
}
