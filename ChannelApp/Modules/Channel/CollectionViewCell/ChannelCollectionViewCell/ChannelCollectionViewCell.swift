import UIKit

final class ChannelCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var channelImageView: UIImageView!
    @IBOutlet weak var channelView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        channelView.viewCorner(10)
    }
    
    func setupSelectedChannel(_ isSelected: Bool) {
        setupChannelShadow(isSelected)
        setupImageView(isSelected)
    }
    
    func setupChannelShadow(_ isSelected: Bool) {
        let orangeColor = UIColor.init(red: 214/256, green: 123/256, blue: 41/256, alpha: 1.0).cgColor
        let grayColor = UIColor.init(red: 0/256, green: 0/256, blue: 0/256, alpha: 0.16).cgColor
        channelView.layer.shadowColor = isSelected ? orangeColor : grayColor
        channelView.layer.shadowOpacity =  4
        channelView.layer.shadowOffset = CGSize.zero
        channelView.layer.shadowRadius = 5
        
        channelView.layer.masksToBounds = false
    }
    
    func setupImageView(_ isSelected: Bool) {
        if !isSelected {
            channelImageView.tintColor = .gray
        }
    }
    
    func setupImage(_ url: String?) {
        guard let url = url else { return }
        
        //setup image from  url
    }
}
