import UIKit

final class HeaderView: UIView {
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            setupCollectionView()
        }
    }
    
    var channelList: [Any] = []
    var selectedItem: Int = 0
    
    var chooseChannel: ((Int) -> Void)?
    
    func setupCollectionView() {
        collectionView.register(ChannelCollectionViewCell.self)
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func updateView(_ isMax: Bool, _ maxHeight: CGFloat) {
        let alpha: CGFloat = (maxHeight - self.frame.size.height) / 200
        layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: alpha).cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowOpacity = isMax ? 0 : 1
        layer.masksToBounds = false
    }
}

extension HeaderView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if selectedItem == indexPath.item {
            let size = self.frame.size.height - 86
            return CGSize(width: size, height: size) //86
        }
        let size = self.frame.size.height - 109
        return CGSize(width: size, height: size)  //109
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return channel count after loading
        //        return channelList.count
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(ChannelCollectionViewCell.self, for: indexPath)
        //setup image from model url
//        let item = channelList[indexPath.item]
//        cell.setupImage(item.image)
        
        cell.setupSelectedChannel(indexPath.item == selectedItem)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedItem != indexPath.item {
            selectedItem = indexPath.item
            collectionView.reloadData()
            self.chooseChannel?(indexPath.item)
        }
    }
}
