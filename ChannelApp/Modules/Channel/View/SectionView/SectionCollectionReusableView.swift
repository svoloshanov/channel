import UIKit

class SectionCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var sectionLabel: UILabel!
    
    func setupLabel(_ text: String) {
        sectionLabel.text = text
    }
}
