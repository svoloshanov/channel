import UIKit

protocol ChannelViewControllerProtocol: class {
    func reloadPrograms()
    func reloadChannels()
    func chooseChannel()
}

final class ChannelViewController: UIViewController {
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet private weak var headerViewHeight: NSLayoutConstraint!
    
    var presenter: ChannelPresenterProtocol!
    
    let maxHeaderHeight: CGFloat = 280
    let minHeaderHeight: CGFloat = 220
    var previousScrollOffset: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = ChannelPresenter(view: self)
        setupViews()
    }
    
    func setupViews() {
        presenter.loadChannels()
        setupCollection()
    }
    
    func setupHeaderView() {
        presenter.configurateHeaderView(headerView)
    }
    
    func setupCollection() {
        collectionView.register(ProgramCollectionViewCell.self)
        collectionView.register(SectionCollectionReusableView.self, for: UICollectionView.elementKindSectionHeader)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: UIScreen.main.bounds.size.width / 3, height: 190)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
    }
}
//MARK: - Scroll functions
extension ChannelViewController {
    func canAnimateHeader (_ scrollView: UIScrollView) -> Bool {
        let scrollViewMaxHeight = scrollView.frame.height + self.headerViewHeight.constant - minHeaderHeight
        return scrollView.contentSize.height > scrollViewMaxHeight
    }
    
    func setScrollPosition() {
        collectionView.contentOffset = CGPoint(x:0, y: 0)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollDiff = (scrollView.contentOffset.y - previousScrollOffset)
        let isScrollingDown = scrollDiff > 0
        let isScrollingUp = scrollDiff < 0
        if canAnimateHeader(scrollView) {
            var newHeight = headerViewHeight.constant
            if isScrollingDown {
                newHeight = max(minHeaderHeight, headerViewHeight.constant - abs(scrollDiff))
            } else if isScrollingUp {
                newHeight = min(maxHeaderHeight, headerViewHeight.constant + abs(scrollDiff))
            }
            if newHeight != headerViewHeight.constant {
                headerViewHeight.constant = newHeight
                setScrollPosition()
                let radius = (maxHeaderHeight - newHeight) * 4.5
                headerView.viewCorner(radius)
                previousScrollOffset = scrollView.contentOffset.y
                headerView.updateView(headerViewHeight.constant == maxHeaderHeight, maxHeaderHeight)
            }
        }
    }
}
//MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension ChannelViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getChannelsCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(ProgramCollectionViewCell.self, for: indexPath)
        
        presenter.configurateChannelCell(cell, index: indexPath.item)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableView(SectionCollectionReusableView.self, for: UICollectionView.elementKindSectionHeader, for: indexPath)
        view.setupLabel("Programs")
             
        return view

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 64)
    }
}
//MARK: - ChannelViewControllerProtocol
extension ChannelViewController: ChannelViewControllerProtocol {
    func reloadPrograms() {
        collectionView.reloadData()
    }
    
    func reloadChannels() {
        setupHeaderView()
    }
    
    func chooseChannel() {
        presenter.loadPrograms()
    }
}
