import Foundation

protocol ChannelPresenterProtocol {
    init(view: ChannelViewControllerProtocol)
    func loadChannels()
    func loadPrograms()
    func getChannelsCount() -> Int
    func configurateChannelCell(_ cell: ProgramCollectionViewCell, index: Int)
    func configurateHeaderView(_ headerView: HeaderView)
}

final class ChannelPresenter: ChannelPresenterProtocol {
    
    private weak var view: ChannelViewControllerProtocol?
    
    var channelList: [Any] = [] //Channel Model List
    var programList: [Any] = [] //Program Model List
    var selectedChannel: Int = 1
    
    required init(view: ChannelViewControllerProtocol) {
        self.view = view
    }
    
    func loadChannels() {
        //Request
        view?.reloadChannels()
    }
    
    func loadPrograms() {
        //Request
    }
    
    func getChannelsCount() -> Int {
        //return program count after loading
//        return programList.count
        return 30
    }
    
    func configurateChannelCell(_ cell: ProgramCollectionViewCell, index: Int) {
//        let item = programList[index]
        //setup image and text from model
//        cell.setupImage(item.image)
//        cell.setupLabel(item.text)
    }
    
    func configurateHeaderView(_ headerView: HeaderView) {
        headerView.channelList = channelList
        headerView.selectedItem = selectedChannel
        headerView.chooseChannel =  { [weak self] index in
            guard let `self` = self else { return }
            
            self.selectedChannel = index
            self.view?.chooseChannel()
        }
    }
}
