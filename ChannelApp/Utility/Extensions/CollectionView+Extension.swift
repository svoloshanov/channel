import UIKit

extension UICollectionReusableView: NibLoadable {}

protocol NibLoadable {
    static var nib: UINib { get }
    static var identifier: String { get }
}

extension NibLoadable {
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: Bundle.main)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}

extension UINib {
    convenience init<T>(_: T.Type) {
        self.init(nibName: String(describing: T.self), bundle: Bundle.main)
    }
}

extension UICollectionView {
    func register<T: UICollectionViewCell>(_: T.Type) {
        self.register(T.nib, forCellWithReuseIdentifier: T.identifier)
    }
    
    func dequeue<T: UICollectionViewCell>(_: T.Type, for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as! T
    }
    
    func register<T: UICollectionReusableView>(_: T.Type, for supplementaryViewOfKind: String) {
        self.register(T.nib, forSupplementaryViewOfKind: supplementaryViewOfKind, withReuseIdentifier: T.identifier)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(_: T.Type, for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as! T
    }
    
    func dequeueReusableView<T: UICollectionReusableView>(_: T.Type, for supplementaryViewOfKind: String, for indexPath: IndexPath) -> T {
        return self.dequeueReusableSupplementaryView(ofKind: supplementaryViewOfKind, withReuseIdentifier: T.identifier, for: indexPath) as! T
    }
}
